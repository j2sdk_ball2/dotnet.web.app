﻿using Owin;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Dotnet.Web.App
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
               "ApiDefault",
               "api",
                new { controller = "Default", action = "Index", id = RouteParameter.Optional },
                //new string[] { "Dotnet.Web.Api.Controllers" },
                new { httpMethod = new System.Web.Http.Routing.HttpMethodConstraint(HttpMethod.Get) }
            );

            // //  Enable attribute based routing
            // config.MapHttpAttributeRoutes();
            app.UseWebApi(config);

            RouteTable.Routes.MapRoute(
                "Default",
                "",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new string[] { "Dotnet.Web.App.App.Controllers" }
            );

            app.Use(
                (context, next) =>
                    {
                        if (context.Request.Path.HasValue && context.Request.Path.Value.ToLower().Contains("transaction"))
                        {
                            return context.Response.WriteAsync("Under construction");
                        }

                        return next.Invoke();
                    });
        }
    }
}