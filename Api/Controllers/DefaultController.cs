﻿using System.Web.Http;

namespace Dotnet.Web.Api.Controllers
{
    [RoutePrefix("api")]
    public class DefaultController : ApiController
    {
        [HttpGet]
        [Route("")]
        [AllowAnonymous]
        public IHttpActionResult Index()
        {
            return Ok(new
            {
                name = "Demo API",
                path = "http://[ip_address]:[port]/api/*",
                version = "1.0.0"
            });
        }
    }
}