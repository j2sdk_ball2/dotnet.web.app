﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Dotnet.Web.App.App.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return Json(new
            {
                name = "Demo App",
                path = "http://[ip_address]:[port]/*",
                version = "1.0.0"
            },
            JsonRequestBehavior.AllowGet);
        }
    }
}